'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    return queryInterface.bulkInsert('Users', [{
      name: 'Muhammad Akbar',
      address: "Pondok Kelapa",
      age: 23,
      job: 'mahasiswa',
      city: 'Jakarta',
      email: 'barmuhammad44@gmail.com',
      createdAt: new Date(),
      updatedAt: new Date(),
    },
    {
      name: 'Ramdan',
      address: "Jatibening",
      age: 27,
      job: 'mahasiswa',
      city: 'Bekasi',
      email: 'ramdan94369739@gmail.com',
      createdAt: new Date(),
      updatedAt: new Date(),
    },
    {
      name: 'Irfan',
      address: "Pondok Bambu",
      age: 25,
      job: 'Programmer',
      city: 'Jakarta',
      email: 'irfan3719@gmail.com',
      createdAt: new Date(),
      updatedAt: new Date(),
    },
    {
      name: 'Udin',
      address: "Duren Tiga",
      age: 30,
      job: 'Buruh',
      city: 'Jakarta',
      email: 'udin21503728@gmail.com',
      createdAt: new Date(),
      updatedAt: new Date(),
    },
    {
      name: 'Ihsan',
      address: "Pondok Kelapa",
      age: 25,
      job: 'mahasiswa',
      city: 'Bekasi',
      email: 'ihsan4242@gmail.com',
      createdAt: new Date(),
      updatedAt: new Date(),
    },
    ]);
  },

  async down (queryInterface, Sequelize) {
    return queryInterface.bulkDelete('Users', null, {});
  }
};
