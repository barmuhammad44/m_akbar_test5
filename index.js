const express = require('express')
const app = express()
require('dotenv').config()
// const port = 3000
const port = process.env.PORT
const bodyParser=require('body-parser')
const userRoute = require('./route/user_route')

app.use(bodyParser.urlencoded({
    extended: true
  }));

app.use(bodyParser.json());

// app.get('/', (req,res)=> res.send('halo dunia'))
app.use('/user', userRoute)

app.listen(port, ()=> console.log(`listening port ${port}`))